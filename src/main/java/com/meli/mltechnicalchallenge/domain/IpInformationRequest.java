package com.meli.mltechnicalchallenge.domain;

import java.io.Serializable;
import lombok.Data;

@Data
public class IpInformationRequest implements Serializable {

  private static final long serialVersionUID = 7916506396388463051L;

  private String ipAddress;
}
