package com.meli.mltechnicalchallenge.domain;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CurrencyData implements Serializable {

  private static final long serialVersionUID = 5741779637369462363L;

  private String code;
  private String name;
  private String symbol;
}
