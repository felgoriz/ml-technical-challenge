package com.meli.mltechnicalchallenge.domain;

import java.io.Serializable;
import lombok.Data;

@Data
public class IpBanRequest implements Serializable {

  private static final long serialVersionUID = 3788106300591719978L;

  private String ipAddress;
}
