package com.meli.mltechnicalchallenge.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CurrencyConversionData implements Serializable {

  private static final long serialVersionUID = 7214255425100806310L;

  private Boolean success;
  private Integer timestamp;
  private String base;
  private String date;
  private Map<String, BigDecimal> rates;
}
