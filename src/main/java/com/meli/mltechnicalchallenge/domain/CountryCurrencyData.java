package com.meli.mltechnicalchallenge.domain;

import java.io.Serializable;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CountryCurrencyData implements Serializable {

  private static final long serialVersionUID = 1927538861259326293L;

  List<CurrencyData> currencies;
}
