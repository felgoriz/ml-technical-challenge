package com.meli.mltechnicalchallenge.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IpInformationResponse implements Serializable {

  private static final long serialVersionUID = 2269477828490112031L;

  private String countryName;
  private String countryIsoCode;
  private String localCurrency;
  private BigDecimal dollarsCurrencyValue;
  private BigDecimal eurosCurrencyValue;
}
