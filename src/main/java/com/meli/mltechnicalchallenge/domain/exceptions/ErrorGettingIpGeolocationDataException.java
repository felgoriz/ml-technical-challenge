package com.meli.mltechnicalchallenge.domain.exceptions;

public class ErrorGettingIpGeolocationDataException extends RuntimeException {

  private static final long serialVersionUID = 6937985366395640018L;

  public ErrorGettingIpGeolocationDataException(final String message) {
    super(message);
  }

  public ErrorGettingIpGeolocationDataException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
