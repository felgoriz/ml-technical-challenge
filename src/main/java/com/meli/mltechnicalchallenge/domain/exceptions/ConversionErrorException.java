package com.meli.mltechnicalchallenge.domain.exceptions;

public class ConversionErrorException extends RuntimeException {

  private static final long serialVersionUID = 5615796188427804324L;

  public ConversionErrorException(final String message) {
    super(message);
  }

  public ConversionErrorException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
