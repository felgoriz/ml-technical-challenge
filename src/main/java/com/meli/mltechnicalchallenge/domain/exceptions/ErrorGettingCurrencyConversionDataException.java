package com.meli.mltechnicalchallenge.domain.exceptions;

public class ErrorGettingCurrencyConversionDataException extends RuntimeException {

  private static final long serialVersionUID = 3651572122736964994L;

  public ErrorGettingCurrencyConversionDataException(final String message) {
    super(message);
  }

  public ErrorGettingCurrencyConversionDataException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
