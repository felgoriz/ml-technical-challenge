package com.meli.mltechnicalchallenge.domain.exceptions;

public class ErrorGettingCountryCurrencyDataException extends RuntimeException {

  private static final long serialVersionUID = 1910459489436685754L;

  public ErrorGettingCountryCurrencyDataException(final String message) {
    super(message);
  }

  public ErrorGettingCountryCurrencyDataException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
