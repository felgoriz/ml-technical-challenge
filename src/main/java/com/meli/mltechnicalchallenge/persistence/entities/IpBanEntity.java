package com.meli.mltechnicalchallenge.persistence.entities;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.redis.core.RedisHash;

@Data
@Builder
@RedisHash("ban:")
public class IpBanEntity {

  private String id;
}
