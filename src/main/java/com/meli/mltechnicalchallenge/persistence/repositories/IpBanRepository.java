package com.meli.mltechnicalchallenge.persistence.repositories;

import com.meli.mltechnicalchallenge.persistence.entities.IpBanEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IpBanRepository extends CrudRepository<IpBanEntity, String> {}
