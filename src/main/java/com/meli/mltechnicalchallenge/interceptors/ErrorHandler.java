package com.meli.mltechnicalchallenge.interceptors;

import com.meli.mltechnicalchallenge.domain.ErrorResponse;
import com.meli.mltechnicalchallenge.domain.exceptions.BannedIpErrorException;
import com.meli.mltechnicalchallenge.domain.exceptions.ConversionErrorException;
import com.meli.mltechnicalchallenge.domain.exceptions.ErrorGettingCountryCurrencyDataException;
import com.meli.mltechnicalchallenge.domain.exceptions.ErrorGettingCurrencyConversionDataException;
import com.meli.mltechnicalchallenge.domain.exceptions.ErrorGettingIpGeolocationDataException;
import java.time.LocalDateTime;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(BannedIpErrorException.class)
  public ResponseEntity<ErrorResponse> handleBannedIpError(final Exception ex) {
    return buildErrorResponse(HttpStatus.FORBIDDEN, ex);
  }

  @ExceptionHandler({
    ErrorGettingIpGeolocationDataException.class,
    ErrorGettingCountryCurrencyDataException.class,
    ErrorGettingCurrencyConversionDataException.class
  })
  public ResponseEntity<ErrorResponse> handleExternalServiceError(final Exception ex) {
    return buildErrorResponse(HttpStatus.BAD_GATEWAY, ex);
  }

  @ExceptionHandler(ConversionErrorException.class)
  public ResponseEntity<ErrorResponse> handleConversionError(final Exception ex) {
    return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex);
  }

  private ResponseEntity<ErrorResponse> buildErrorResponse(
      final HttpStatus httpStatus, final Exception ex) {
    final ErrorResponse errorResponse =
        ErrorResponse.builder()
            .timestamp(LocalDateTime.now())
            .error(ex.getMessage())
            .status(httpStatus.value())
            .build();
    return new ResponseEntity<>(errorResponse, httpStatus);
  }
}
