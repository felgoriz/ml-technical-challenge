package com.meli.mltechnicalchallenge.controllers;

import com.meli.mltechnicalchallenge.domain.IpBanRequest;
import com.meli.mltechnicalchallenge.domain.IpInformationRequest;
import com.meli.mltechnicalchallenge.domain.IpInformationResponse;
import com.meli.mltechnicalchallenge.services.NetworkingService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RequestMapping("/networking")
@RestController
public class NetworkingControllerImpl implements NetworkingController {

  @Autowired NetworkingService networkingService;

  @PostMapping("/ip")
  @Override
  public IpInformationResponse getIpInformation(final @RequestBody IpInformationRequest request) {
    log.info("Processing request {}", request);
    return networkingService.getIpInformation(request);
  }

  @PutMapping("/ip")
  @Override
  public void banIp(final @RequestBody IpBanRequest request) {
    log.info("Processing request {}", request);
    networkingService.banIp(request);
  }
}
