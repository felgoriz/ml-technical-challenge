package com.meli.mltechnicalchallenge.controllers;

import com.meli.mltechnicalchallenge.domain.IpBanRequest;
import com.meli.mltechnicalchallenge.domain.IpInformationRequest;
import com.meli.mltechnicalchallenge.domain.IpInformationResponse;

public interface NetworkingController {

  IpInformationResponse getIpInformation(IpInformationRequest request);

  void banIp(IpBanRequest request);
}
