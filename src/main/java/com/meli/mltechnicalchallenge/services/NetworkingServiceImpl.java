package com.meli.mltechnicalchallenge.services;

import com.meli.mltechnicalchallenge.consumers.CountryInformationConsumer;
import com.meli.mltechnicalchallenge.consumers.CurrencyConversionConsumer;
import com.meli.mltechnicalchallenge.consumers.IpGeolocationConsumer;
import com.meli.mltechnicalchallenge.domain.CountryCurrencyData;
import com.meli.mltechnicalchallenge.domain.CurrencyConversionData;
import com.meli.mltechnicalchallenge.domain.CurrencyData;
import com.meli.mltechnicalchallenge.domain.IpBanRequest;
import com.meli.mltechnicalchallenge.domain.IpGeolocationData;
import com.meli.mltechnicalchallenge.domain.IpInformationRequest;
import com.meli.mltechnicalchallenge.domain.IpInformationResponse;
import com.meli.mltechnicalchallenge.domain.exceptions.BannedIpErrorException;
import com.meli.mltechnicalchallenge.domain.exceptions.ConversionErrorException;
import com.meli.mltechnicalchallenge.persistence.entities.IpBanEntity;
import com.meli.mltechnicalchallenge.persistence.repositories.IpBanRepository;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class NetworkingServiceImpl implements NetworkingService {

  @Value("${currency.roundingScale}")
  private Integer roundingScale;

  @Value("${currency.dollarCode}")
  private String dollarCode;

  @Value("${currency.euroCode}")
  private String euroCode;

  @Autowired private IpBanRepository ipBanRepository;
  @Autowired private IpGeolocationConsumer ipGeolocationConsumer;
  @Autowired private CountryInformationConsumer countryInformationConsumer;
  @Autowired private CurrencyConversionConsumer currencyConversionConsumer;

  @Cacheable(value = "info", key = "#request.ipAddress")
  @Override
  public IpInformationResponse getIpInformation(final IpInformationRequest request) {
    log.info("Getting information of ip {}", request.getIpAddress());
    validateIfIpIsBanned(request.getIpAddress());
    final IpGeolocationData ipGeolocationData =
        ipGeolocationConsumer.getIpGeolocationData(request.getIpAddress());
    log.info(ipGeolocationData);
    final CountryCurrencyData countryCurrencyData =
        countryInformationConsumer.getCountryCurrencyData(ipGeolocationData.getCountryCode3());
    log.info(countryCurrencyData);
    final CurrencyData currencyData = countryCurrencyData.getCurrencies().get(0);
    final CurrencyConversionData currencyConversionData =
        currencyConversionConsumer.getCurrencyConversionData(currencyData.getCode());
    log.info(currencyConversionData);
    final Map<String, BigDecimal> rates = currencyConversionData.getRates();
    final IpInformationResponse response =
        IpInformationResponse.builder()
            .countryName(ipGeolocationData.getCountryName())
            .countryIsoCode(ipGeolocationData.getCountryCode3())
            .localCurrency(currencyData.getName())
            .dollarsCurrencyValue(convertCurrency(rates, currencyData.getCode(), dollarCode))
            .eurosCurrencyValue(convertCurrency(rates, currencyData.getCode(), euroCode))
            .build();
    log.info(response);
    return response;
  }

  @CacheEvict(value = "info", key = "#request.ipAddress")
  @Override
  public void banIp(final IpBanRequest request) {
    log.info("Banning ip {}", request.getIpAddress());
    ipBanRepository.save(IpBanEntity.builder().id(request.getIpAddress()).build());
  }

  private void validateIfIpIsBanned(final String ipAddress) {
    ipBanRepository
        .findById(ipAddress)
        .ifPresent(
            ipBan -> {
              if (null != ipBan.getId()) {
                final String errorMessage =
                    MessageFormat.format("The ip {0} is banned", ipBan.getId());
                log.error(errorMessage);
                throw new BannedIpErrorException(errorMessage);
              }
            });
  }

  private BigDecimal convertCurrency(
      final Map<String, BigDecimal> rates,
      final String fromCurrencyCode,
      final String toCurrencyCode) {
    try {
      return rates
          .get(toCurrencyCode)
          .divide(rates.get(fromCurrencyCode), roundingScale, RoundingMode.HALF_UP);
    } catch (final Exception ex) {
      final String errorMessage =
          MessageFormat.format(
              "Error converting from {0} to {1}", fromCurrencyCode, toCurrencyCode);
      log.error(errorMessage, ex);
      throw new ConversionErrorException(errorMessage, ex);
    }
  }
}
