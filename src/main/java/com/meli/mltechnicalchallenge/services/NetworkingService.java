package com.meli.mltechnicalchallenge.services;

import com.meli.mltechnicalchallenge.domain.IpBanRequest;
import com.meli.mltechnicalchallenge.domain.IpInformationRequest;
import com.meli.mltechnicalchallenge.domain.IpInformationResponse;

public interface NetworkingService {

  IpInformationResponse getIpInformation(IpInformationRequest request);

  void banIp(IpBanRequest request);
}
