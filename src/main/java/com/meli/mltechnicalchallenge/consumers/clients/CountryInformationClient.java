package com.meli.mltechnicalchallenge.consumers.clients;

import com.meli.mltechnicalchallenge.domain.CountryCurrencyData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CountryInformationClient {

  @GET("{countryCode}?fields=currencies")
  Call<CountryCurrencyData> getCountryCurrencyData(@Path("countryCode") String countryCode);
}
