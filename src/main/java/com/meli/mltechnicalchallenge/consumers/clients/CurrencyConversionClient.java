package com.meli.mltechnicalchallenge.consumers.clients;

import com.meli.mltechnicalchallenge.domain.CurrencyConversionData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CurrencyConversionClient {

  @GET("latest")
  Call<CurrencyConversionData> getCurrencyConversionData(
      @Query("access_key") String accessKey, @Query("symbols") String symbols);
}
