package com.meli.mltechnicalchallenge.consumers.clients;

import com.meli.mltechnicalchallenge.domain.IpGeolocationData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface IpGeolocationClient {

  @GET()
  Call<IpGeolocationData> getIpGeolocationData(@Url() String url);
}
