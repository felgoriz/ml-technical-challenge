package com.meli.mltechnicalchallenge.consumers;

import com.meli.mltechnicalchallenge.domain.CurrencyConversionData;

public interface CurrencyConversionConsumer {

  CurrencyConversionData getCurrencyConversionData(String currencyCode);
}
