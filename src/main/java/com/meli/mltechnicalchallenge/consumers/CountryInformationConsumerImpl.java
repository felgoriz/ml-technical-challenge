package com.meli.mltechnicalchallenge.consumers;

import com.meli.mltechnicalchallenge.consumers.clients.CountryInformationClient;
import com.meli.mltechnicalchallenge.domain.CountryCurrencyData;
import com.meli.mltechnicalchallenge.domain.exceptions.ErrorGettingCountryCurrencyDataException;
import java.text.MessageFormat;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class CountryInformationConsumerImpl implements CountryInformationConsumer {

  @Autowired private CountryInformationClient client;

  @Override
  public CountryCurrencyData getCountryCurrencyData(final String countryCode) {
    try {
      return client.getCountryCurrencyData(countryCode).execute().body();
    } catch (final Exception ex) {
      final String errorMessage =
          MessageFormat.format(
              "An error has occurred when trying to obtain the currency data of the country {0}",
              countryCode);
      log.error(errorMessage, ex);
      throw new ErrorGettingCountryCurrencyDataException(errorMessage, ex);
    }
  }
}
