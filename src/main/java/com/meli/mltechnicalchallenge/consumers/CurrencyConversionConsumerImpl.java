package com.meli.mltechnicalchallenge.consumers;

import com.meli.mltechnicalchallenge.consumers.clients.CurrencyConversionClient;
import com.meli.mltechnicalchallenge.domain.CurrencyConversionData;
import com.meli.mltechnicalchallenge.domain.exceptions.ErrorGettingCurrencyConversionDataException;
import java.text.MessageFormat;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class CurrencyConversionConsumerImpl implements CurrencyConversionConsumer {

  @Value("${currency.dollarCode}")
  private String dollarCode;

  @Value("${currency.euroCode}")
  private String euroCode;

  @Value("${external.fixerAccessKey}")
  private String fixerAccessKey;

  @Autowired private CurrencyConversionClient client;

  @Override
  public CurrencyConversionData getCurrencyConversionData(final String currencyCode) {
    try {
      final String symbols =
          dollarCode.concat(",").concat(euroCode).concat(",").concat(currencyCode);
      return client.getCurrencyConversionData(fixerAccessKey, symbols).execute().body();
    } catch (final Exception ex) {
      final String errorMessage =
          MessageFormat.format(
              "An error has occurred when trying to obtain the conversion data of the currency {0}",
              currencyCode);
      log.error(errorMessage, ex);
      throw new ErrorGettingCurrencyConversionDataException(errorMessage, ex);
    }
  }
}
