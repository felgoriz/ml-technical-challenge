package com.meli.mltechnicalchallenge.consumers;

import com.meli.mltechnicalchallenge.consumers.clients.IpGeolocationClient;
import com.meli.mltechnicalchallenge.domain.IpGeolocationData;
import com.meli.mltechnicalchallenge.domain.exceptions.ErrorGettingIpGeolocationDataException;
import java.text.MessageFormat;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class IpGeolocationConsumerImpl implements IpGeolocationConsumer {

  private static final String IP_GEOLOCATION_PATH = "ip?";

  @Value("${external.ip2CountryUrl}")
  private String ip2CountryUrl;

  @Autowired private IpGeolocationClient client;

  @Override
  public IpGeolocationData getIpGeolocationData(final String ipAddress) {
    try {
      final String url = ip2CountryUrl.concat(IP_GEOLOCATION_PATH).concat(ipAddress);
      return client.getIpGeolocationData(url).execute().body();
    } catch (final Exception ex) {
      final String errorMessage =
          MessageFormat.format(
              "An error has occurred when trying to obtain the geolocation data of the ip {0}",
              ipAddress);
      log.error(errorMessage, ex);
      throw new ErrorGettingIpGeolocationDataException(errorMessage, ex);
    }
  }
}
