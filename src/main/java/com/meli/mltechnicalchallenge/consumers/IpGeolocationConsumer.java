package com.meli.mltechnicalchallenge.consumers;

import com.meli.mltechnicalchallenge.domain.IpGeolocationData;

public interface IpGeolocationConsumer {

  IpGeolocationData getIpGeolocationData(String ipAddress);
}
