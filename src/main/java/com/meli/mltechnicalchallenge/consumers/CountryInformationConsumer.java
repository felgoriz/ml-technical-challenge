package com.meli.mltechnicalchallenge.consumers;

import com.meli.mltechnicalchallenge.domain.CountryCurrencyData;

public interface CountryInformationConsumer {

  CountryCurrencyData getCountryCurrencyData(String countryCode);
}
