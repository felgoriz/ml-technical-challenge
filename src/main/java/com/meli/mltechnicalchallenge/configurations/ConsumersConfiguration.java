package com.meli.mltechnicalchallenge.configurations;

import com.meli.mltechnicalchallenge.consumers.clients.CountryInformationClient;
import com.meli.mltechnicalchallenge.consumers.clients.CurrencyConversionClient;
import com.meli.mltechnicalchallenge.consumers.clients.IpGeolocationClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Configuration
public class ConsumersConfiguration {

  @Value("${external.ip2CountryUrl}")
  private String ip2CountryUrl;

  @Value("${external.restCountriesUrl}")
  private String restCountriesUrl;

  @Value("${external.fixerUrl}")
  private String fixerUrl;

  @Bean
  public IpGeolocationClient buildIpGeolocationClient() {
    final Retrofit retrofit =
        new Retrofit.Builder()
            .baseUrl(ip2CountryUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    return retrofit.create(IpGeolocationClient.class);
  }

  @Bean
  public CountryInformationClient buildCountryInformationClient() {
    final Retrofit retrofit =
        new Retrofit.Builder()
            .baseUrl(restCountriesUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    return retrofit.create(CountryInformationClient.class);
  }

  @Bean
  public CurrencyConversionClient buildCurrencyConversionClient() {
    final Retrofit retrofit =
        new Retrofit.Builder()
            .baseUrl(fixerUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    return retrofit.create(CurrencyConversionClient.class);
  }
}
