package com.meli.mltechnicalchallenge.configurations;

import java.time.Duration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@EnableCaching
@Configuration
public class JedisConfiguration {

  @Value("${caching.redisHostName}")
  private String redisHostName;

  @Value("${caching.redisPort}")
  private Integer redisPort;

  @Value("${caching.redisTtlInSeconds}")
  private Integer redisTtlInSeconds;

  @Bean
  public JedisConnectionFactory jedisConnectionFactory() {
    final RedisStandaloneConfiguration redisStandaloneConfiguration =
        new RedisStandaloneConfiguration(redisHostName, redisPort);
    return new JedisConnectionFactory(redisStandaloneConfiguration);
  }

  @Bean
  public RedisTemplate<String, Object> redisTemplate() {
    final RedisTemplate<String, Object> template = new RedisTemplate<>();
    template.setConnectionFactory(jedisConnectionFactory());
    return template;
  }

  @Bean
  public RedisCacheConfiguration cacheConfiguration() {
    return RedisCacheConfiguration.defaultCacheConfig()
        .entryTtl(Duration.ofSeconds(redisTtlInSeconds))
        .disableCachingNullValues();
  }

  @Bean
  public RedisCacheManager cacheManager() {
    return RedisCacheManager.builder(jedisConnectionFactory())
        .cacheDefaults(cacheConfiguration())
        .transactionAware()
        .build();
  }
}
