package com.meli.mltechnicalchallenge.controllers;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meli.mltechnicalchallenge.domain.IpBanRequest;
import com.meli.mltechnicalchallenge.domain.IpInformationRequest;
import com.meli.mltechnicalchallenge.domain.IpInformationResponse;
import com.meli.mltechnicalchallenge.services.NetworkingService;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(NetworkingController.class)
class NetworkingControllerImplTest {

  @Autowired private MockMvc mockMvc;

  @MockBean private NetworkingService service;

  @Test
  void getIpInformationTest() throws Exception {
    // Given
    final IpInformationRequest request = new IpInformationRequest();
    request.setIpAddress("23.7.144.77");
    final IpInformationResponse response =
        IpInformationResponse.builder()
            .countryName("Colombia")
            .countryIsoCode("COL")
            .localCurrency("Colombian peso")
            .dollarsCurrencyValue(new BigDecimal("0.00027"))
            .eurosCurrencyValue(new BigDecimal("0.00022"))
            .build();
    when(service.getIpInformation(any(IpInformationRequest.class))).thenReturn(response);
    // Then
    this.mockMvc
        .perform(
            post("/networking/ip")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(request))
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.countryName", is("Colombia")))
        .andExpect(jsonPath("$.countryIsoCode", is("COL")))
        .andExpect(jsonPath("$.localCurrency", is("Colombian peso")));
  }

  @Test
  void banIpTest() throws Exception {
    // Given
    final IpBanRequest request = new IpBanRequest();
    request.setIpAddress("23.7.144.77");
    doNothing().when(service).banIp(any(IpBanRequest.class));
    // Then
    this.mockMvc
        .perform(
            put("/networking/ip")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(request)))
        .andExpect(status().isOk());
  }

  private String asJsonString(final Object object) throws JsonProcessingException {
    final ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.writeValueAsString(object);
  }
}
