package com.meli.mltechnicalchallenge.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.meli.mltechnicalchallenge.consumers.CountryInformationConsumer;
import com.meli.mltechnicalchallenge.consumers.CurrencyConversionConsumer;
import com.meli.mltechnicalchallenge.consumers.IpGeolocationConsumer;
import com.meli.mltechnicalchallenge.domain.CountryCurrencyData;
import com.meli.mltechnicalchallenge.domain.CurrencyConversionData;
import com.meli.mltechnicalchallenge.domain.CurrencyData;
import com.meli.mltechnicalchallenge.domain.IpBanRequest;
import com.meli.mltechnicalchallenge.domain.IpGeolocationData;
import com.meli.mltechnicalchallenge.domain.IpInformationRequest;
import com.meli.mltechnicalchallenge.domain.IpInformationResponse;
import com.meli.mltechnicalchallenge.domain.exceptions.BannedIpErrorException;
import com.meli.mltechnicalchallenge.persistence.entities.IpBanEntity;
import com.meli.mltechnicalchallenge.persistence.repositories.IpBanRepository;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
class NetworkingServiceImplTest {

  @Mock private IpBanRepository ipBanRepository;

  @Mock private IpGeolocationConsumer ipGeolocationConsumer;

  @Mock private CountryInformationConsumer countryInformationConsumer;

  @Mock private CurrencyConversionConsumer currencyConversionConsumer;

  @InjectMocks private NetworkingServiceImpl service;

  @Test
  void getIpInformationTest() {
    // Given
    ReflectionTestUtils.setField(service, "roundingScale", 5);
    ReflectionTestUtils.setField(service, "dollarCode", "USD");
    ReflectionTestUtils.setField(service, "euroCode", "EUR");
    final IpInformationRequest request = new IpInformationRequest();
    request.setIpAddress("23.7.144.77");
    final IpInformationResponse expectedResponse =
        IpInformationResponse.builder()
            .countryName("Colombia")
            .countryIsoCode("COL")
            .localCurrency("Colombian peso")
            .dollarsCurrencyValue(new BigDecimal("0.00027"))
            .eurosCurrencyValue(new BigDecimal("0.00022"))
            .build();
    when(ipBanRepository.findById(any(String.class))).thenReturn(Optional.empty());
    final IpGeolocationData ipGeolocationData =
        IpGeolocationData.builder()
            .countryCode("CO")
            .countryCode3("COL")
            .countryName("Colombia")
            .countryEmoji("????")
            .build();
    when(ipGeolocationConsumer.getIpGeolocationData(any(String.class)))
        .thenReturn(ipGeolocationData);
    final CountryCurrencyData countryCurrencyData =
        CountryCurrencyData.builder()
            .currencies(
                Collections.singletonList(
                    CurrencyData.builder().code("COP").name("Colombian peso").symbol("$").build()))
            .build();
    when(countryInformationConsumer.getCountryCurrencyData(any(String.class)))
        .thenReturn(countryCurrencyData);
    final CurrencyConversionData currencyConversionData =
        CurrencyConversionData.builder()
            .success(true)
            .timestamp(1597382646)
            .base("EUR")
            .date("2020-08-14")
            .rates(
                Map.of(
                    "USD",
                    new BigDecimal("1.18185"),
                    "EUR",
                    new BigDecimal("1"),
                    "COP",
                    new BigDecimal("4457.938546")))
            .build();
    when(currencyConversionConsumer.getCurrencyConversionData(any(String.class)))
        .thenReturn(currencyConversionData);
    // When
    final IpInformationResponse response = service.getIpInformation(request);
    // Then
    assertEquals(expectedResponse, response);
  }

  @Test
  void getIpInformationWithIpBannedTest() {
    // Given
    final IpInformationRequest request = new IpInformationRequest();
    request.setIpAddress("23.7.144.77");
    when(ipBanRepository.findById(any(String.class)))
        .thenReturn(Optional.of(IpBanEntity.builder().id("23.7.144.77").build()));
    // Then
    assertThrows(BannedIpErrorException.class, () -> service.getIpInformation(request));
  }

  @Test
  void banIpTest() {
    // Given
    final IpBanRequest request = new IpBanRequest();
    request.setIpAddress("23.7.144.77");
    final IpBanEntity entity = IpBanEntity.builder().id("23.7.144.77").build();
    when(ipBanRepository.save(any(IpBanEntity.class))).thenReturn(entity);
    // When
    service.banIp(request);
    // Then
    verify(ipBanRepository, times(1)).save(entity);
  }
}
