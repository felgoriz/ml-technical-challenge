**Explicación**

Para la solución del challenge se planteó una solución empleando *Java 11* con el framework *Spring Boot 2*.
Esta consiste en un api rest que expone dos recursos, uno para consultar la información de la dirección ip
y otra para agregarla a la lista negra. La información de la ip se obtiene consumiendo los api's sugeridos
en secuencia y procesando la data de tasas de cambio para conseguir el valor de cambio final. El sistema
hace uso de *Redis* para almacenamiento de datos, la información de cada ip que se consulta se guarda en
cache durante 1 hora y la lista de ip's que se agregan a la lista negra son persistidas en un hash set.

**Tecnologías**

- [JDK 11](https://www.azul.com/downloads/zulu-community/?version=java-11-lts&architecture=x86-64-bit&package=jdk)
(Se uso la versión mas reciente de Zulu OpenJDK 11)
- [Maven 3.6.3](https://maven.apache.org/download.cgi)
- [Spring Boot 2.3.2](https://github.com/spring-projects/spring-boot/tree/v2.3.2.RELEASE)
- [Redis](https://redis.io/)

**Uso**

 1. Instalar JDK y Redis

 2. Configurar si es necesario los puertos de Spring y Redis en las propiedades:

	`src/main/resources/application.properties`

 3. Compilar y ejecutar el proyecto con el siguiente comando:

	 `./mvnw package && java -jar target/ml-technical-challenge-0.0.1-SNAPSHOT.jar`

 4. Las siguientes son las peticiones correspondientes a los dos recursos:

    a) Consultar la información de una dirección ip

		curl --location --request POST 'http://localhost:8082/networking/ip' --header 'Content-Type: application/json' --data-raw '{"ipAddress": "186.84.110.107"}'

	b) Agregar una dirección ip a la lista negra

		curl --location --request PUT 'http://localhost:8082/networking/ip' --header 'Content-Type: application/json' --data-raw '{"ipAddress": "186.84.110.108"}'

***Nota:*** Se agregó un archivo Dockerfile que permite ejecutar el servicio desplegandolo en un contenedor de Docker, para ello se deben emplear los siguientes comandos:

````
docker build -t meli/ml-technical-challenge .
docker run -p 8082:8082 meli/ml-technical-challenge
````
````
